FROM elixir:1.9-alpine

# Base requirement
RUN apk add git nodejs npm inotify-tools

# Required when building node dependencies node-gyp (needed on ARM)
RUN apk add python make g++

# Setup user for the app
ENV UID=1000
ENV GID=1000

RUN addgroup -S phoenix --gid $GID && \
  adduser -S phoenix -G phoenix -u $UID

# Setup mix enviropnment
USER phoenix
RUN mix local.hex --force && \
  mix local.rebar --force && \
  mix archive.install --force hex phx_new 1.4.9

# Go home
WORKDIR /home/phoenix
